package main

import (
	"crypto/ed25519"
	"crypto/rand"
	"encoding/hex"
	"flag"
	"fmt"
	"os"
)

const (
	privateKeySize = 32
	publicKeySize  = 32
)

var outFile *string = flag.String("o", "", "file to save the private key to (corresponding public key written to key.pub). If no file is specified, private key is written to stdout")

func main() {
	flag.Parse()
	if len(flag.Args()) > 0 {
		fmt.Fprintln(os.Stderr, "Usage: keygen [-o FILE]")
		os.Exit(1)
	}
	pub, expanded_priv, err := ed25519.GenerateKey(rand.Reader)
	if err != nil {
		fmt.Fprintf(os.Stderr, "generate key:", err)
		os.Exit(1)
	}
	priv := expanded_priv.Seed()

	if len(pub) != privateKeySize {
		panic(fmt.Errorf("unexpected public key size: ", len(pub)))
		os.Exit(1)
	}
	if len(priv) != privateKeySize {
		panic(fmt.Errorf("unexpected private key size: ", len(priv)))
		os.Exit(1)
	}
	if len(*outFile) > 0 {
		writeHexToFile(*outFile, 0600, priv)
		writeHexToFile(*outFile+".pub", 0644, pub)
	} else {
		fmt.Println(hex.EncodeToString(priv))
		return
	}
}

func writeHexToFile(fileName string, mode os.FileMode, blob []byte) {
	file, err := os.OpenFile(fileName, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, mode)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to open key file '%v': %v", fileName, err)
		os.Exit(1)
	}
	_, err = fmt.Fprintln(file, hex.EncodeToString(blob))
	if err != nil {
		fmt.Fprintf(os.Stderr, "write failed to key file '%v': %v", fileName, err)
		os.Exit(1)
	}
}
