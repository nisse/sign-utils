package main

import (
	"bytes"
	"crypto/ed25519"
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"flag"
	"fmt"
	"io"
	"os"
	"strings"
)

var keyFile *string = flag.String("key", "", "file name of hex-encoded private key to use")
var shardHint *uint64 = flag.Uint64("shard-hint", 0, "shard hint for target log")

var prefix = [51]byte{
	83, 83, 72, 83, 73, 71, // SSHSIG
	0, 0, 0, 0, // reserved
	0, 0, 0, 23, // namespace length
	116, 114, 101, 101, 116, 114, 101, 101, 100, 58, 118, 48, // "tree_leaf:v0"
	64, 115, 105, 103, 115, 117, 109, 46, 111, 114, 103, // "@sigsum.org"
	0, 0, 0, 6, // hash algorithm length
	115, 104, 97, 50, 53, 54, // "sha256"
	0, 0, 0, 32, // hash length
}

func readKey(fileName string) ed25519.PrivateKey {
	keyBytes, err := os.ReadFile(fileName)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to read file `%v': %v", fileName, err)
		os.Exit(1)
	}
	keyHex := string(bytes.TrimSpace(keyBytes))
	key, err := hex.DecodeString(keyHex)
	if err != nil || len(key) != ed25519.SeedSize {
		fmt.Fprintf(os.Stderr, "invalid key `%q': %v", keyHex, err)
		os.Exit(1)
	}
	return ed25519.NewKeyFromSeed(key)
}

func main() {
	flag.Parse()
	if len(*keyFile) == 0 || *shardHint == 0 || len(flag.Args()) > 1 {
		fmt.Println("Usage: sign-leaf --key=KEY-FILE --shard-hint=NUMBER [MSG]")
		os.Exit(1)
	}

	privateKey := readKey(*keyFile)
	var msg []byte
	var msgHex string
	var err error

	if len(flag.Args()) == 0 {
		msg, err = io.ReadAll(os.Stdin)
		if err != nil {
			fmt.Fprintf(os.Stderr, "failed to read stdin: %v", err)
			os.Exit(1)
		}
		if len(msg) != 32 {
			fmt.Fprintf(os.Stderr, "invalid message length %v, must be 32")
			os.Exit(1)
		}
		msgHex = hex.EncodeToString(msg)
	} else {
		msgHex = strings.TrimSpace(flag.Args()[0])
		msg, err = hex.DecodeString(msgHex)
		if err != nil || len(msg) != 32 {
			fmt.Fprintf(os.Stderr, "invalid hex message: %q", msgHex)
			os.Exit(1)
		}
	}

	var shardHintBytes [8]byte
	binary.BigEndian.PutUint64(shardHintBytes[:], *shardHint)

	hash := sha256.Sum256(append(shardHintBytes[:], msg...))

	signature := ed25519.Sign(privateKey, append(prefix[:], hash[:]...))
	fmt.Printf("shard_hint=%d\n", *shardHint)
	fmt.Printf("message=%v\n", msgHex)
	fmt.Printf("signature=%v\n", hex.EncodeToString(signature))
	fmt.Printf("public_key=%v\n", hex.EncodeToString(privateKey.Public().(ed25519.PublicKey)))
}
